import axios from 'axios';

const API_KEY = 'a2cece10a398e7a25c1aafe086efa12a';
const ROOT_URL = 'api.openweathermap.org/data/2.5/forecast';

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
    const url = `http://${ROOT_URL}?q=${city},us&appid=${API_KEY}`;
    const request = axios.get(url);

    return {
        type: FETCH_WEATHER,
        payload: request
    }
}